from MapGenerator import *
from PIL import Image

size = 200

map = MapGenerator(size)

image = Image.new('RGB', (size, size))
image.putdata(map.bitmap)
image.show()
