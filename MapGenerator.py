import random, math

class MapGenerator:
    """docstring for MapGenerator"""
    def __init__(self, size = 200):
        self.size = size
        self.grid = [[{'elevation': 0, 'humidity': 0} for x in xrange(self.size)] for y in xrange(self.size)]

        self.terrain()
        self.makeBitmap()

    def terrain(self):
        for i in xrange(self.size / 2):
            self._makeHeightMap()
            self._makeHumidityMap()

    def _makeHeightMap(self):
        hillRadius = random.uniform(0, self.size * 1/5)
        theta = random.uniform(0, 2*math.pi)
        distance = random.uniform(0, self.size / 2 - hillRadius)
        randPointX = int(self.size / 2 + math.cos(theta) * distance)
        randPointY = int(self.size / 2 + math.sin(theta) * distance)

        startX = randPointX - (hillRadius / 2)
        startY = randPointY - (hillRadius / 2)

        if startX < 0: startX = 0
        if startY < 0: startY = 0

        for x in xrange(self.size):
            for y in xrange(self.size):
                z = (hillRadius ** 2) - ( math.pow(randPointX - x, 2) + math.pow(randPointY - y, 2) )
                if z >= 0:
                    self.grid[x][y]['elevation'] += int(z / math.pow(self.size / 16, 2))

    def _makeHumidityMap(self):
        hillRadius = random.uniform(0, self.size * 1/5)

        startX = random.uniform(0, self.size)
        startY = random.uniform(0, self.size)

        if startX < 0: startX = 0
        if startY < 0: startY = 0

        for x in xrange(self.size):
            for y in xrange(self.size):
                z = (hillRadius ** 2) - ( math.pow(startX - x, 2) + math.pow(startY - y, 2) )
                if z >= 0:
                    self.grid[x][y]['humidity'] += int(z / math.pow(self.size / 16, 2))

    def makeBitmap(self):
        self.bitmap = [(0, 0, 0) for x in xrange(self.size ** 2)]

        i = 0
        for x in xrange(self.size):
            for y in xrange(self.size):
                self.bitmap[i] = self.getColorForPoint(x, y)
                i += 1

    def getColorForPoint(self, x, y):
        point = self.grid[x][y]
        color = []

        if(point['elevation'] <= 10): # water
            color = [28, 107, 160]
        elif(point['elevation'] > 50): # snow
            color = [180, 180, 180]
        elif(point['elevation'] > 40 and point['elevation'] <= 50 and point['humidity'] < 15): # dark forest
            color = [40, 73, 2]
        elif(point['humidity'] > 5 and point['elevation'] <= 50): # jungle
            color = [40, 110, 20]
        else: # desert
            color = [255, 240, 165]

        color[0] += point['elevation']
        color[1] += point['elevation']
        color[2] += point['elevation']

        return (color[0], color[1], color[2])
